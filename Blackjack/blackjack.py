# Blackjack

# On this program, user can play blackjack. When you run the program it first asks how much money you want to play with.
# After that, the main window starts where you can play blackjack. On top right corner are rules and how to play
# When you click "cash out", the blackjack window is destroyed and new window pops up where you can see the winnings

from tkinter import *
import random
import time
import winsound

cards = ["2_of_clubs.png", "2_of_diamonds.png", "2_of_hearts.png", "2_of_spades.png",
         "3_of_clubs.png", "3_of_diamonds.png", "3_of_hearts.png", "3_of_spades.png",
         "4_of_clubs.png", "4_of_diamonds.png", "4_of_hearts.png", "4_of_spades.png",
         "5_of_clubs.png", "5_of_diamonds.png", "5_of_hearts.png", "5_of_spades.png",
         "6_of_clubs.png", "6_of_diamonds.png", "6_of_hearts.png", "6_of_spades.png",
         "7_of_clubs.png", "7_of_diamonds.png", "7_of_hearts.png", "7_of_spades.png",
         "8_of_clubs.png", "8_of_diamonds.png", "8_of_hearts.png", "8_of_spades.png",
         "9_of_clubs.png", "9_of_diamonds.png", "9_of_hearts.png", "9_of_spades.png",
         "10_of_clubs.png", "10_of_diamonds.png", "10_of_hearts.png", "10_of_spades.png",
         "10_jack_of_clubs.png", "10_jack_of_diamonds.png", "10_jack_of_hearts.png", "10_jack_of_spades.png",
         "10_queen_of_clubs.png", "10_queen_of_diamonds.png", "10_queen_of_hearts.png", "10_queen_of_spades.png",
         "10_king_of_clubs.png", "10_king_of_diamonds.png", "10_king_of_hearts.png", "10_king_of_spades.png",
         "1,11_ace_of_clubs.png", "1,11_ace_of_diamonds.png", "1,11_ace_of_hearts.png", "1,11_ace_of_spades.png"]
played_cards = []
Dealer_card_number = 1
Player_card_number = 1
dealer_hand_value = 0
player_hand_value = 0
bet = 10
money = 0
start_money = 0


class StartingMoney:
    """ Asks user the amount of money they want to play with. Checks if the input is correct and launches the game"""
    def __init__(self):
        self.__window = Tk()
        self.__window.title("Add money")

        self.__amount_info = Label(self.__window, text="Enter the amount of money you want to play with.")
        self.__amount = Entry(self.__window)
        self.__play = Button(self.__window, text="Let's play", command=self.play)
        self.__error_info = Label(self.__window, text="")

        self.__amount_info.pack()
        self.__amount.pack()
        self.__play.pack()
        self.__error_info.pack()
        self.__amount.focus()

        self.__window.mainloop()

    def play(self):
        global money, start_money
        try:
            start_money = int(self.__amount.get())
        except ValueError:
            self.__error_info.configure(text="Must be number.")
            self.__amount.delete(0, END)
            return

        if 1000000 >= start_money > 0:
            money = self.__amount.get()
            self.__window.destroy()
            game = Blackjack()
            game.start()
        else:
            self.__error_info.configure(text="Must be positive number, above zero and below 1 000 000.")
            self.__amount.delete(0, END)
            return


class EndingMoney:
    """ After user presses "cash out" on main window this windows pops up and tells if the user lost or won money."""
    def __init__(self):
        self.__window = Tk()
        self.__window.title("Cash out")
        self.__window.geometry("300x100")

        self.__info = Label(self.__window, text="")
        self.__info2 = Label(self.__window, text="")
        self.__amount = Label(self.__window, text=abs(money-int(start_money)), font="Courier")
        self.__close = Button(self.__window, text="Close", command=self.end)

        self.__info.pack()
        self.__info2.pack()
        self.__amount.pack()
        self.__close.pack()

        if money-int(start_money) >= 0:
            self.__info.configure(text="Congratulations!", font="Courier")
            self.__info2.configure(text="You won", font="Courier")

        elif money-int(start_money) < 0:
            self.__info.configure(text="Better luck next time.", font="Courier")
            self.__info2.configure(text="You lost", font="Courier")

        self.__window.mainloop()

    def end(self):
        self.__window.destroy()


class Rules:
    def __init__(self):
        self.__window = Tk()
        self.__window.title("Rules")
        rules = "-------------------------------HOW TO PLAY----------------------------------\n" \
                "On the table you can see your money, your current bet, rules and cash out\n" \
                "You can change the bet by clicking +10 and -10.\n" \
                "When ready to play, click Deal. After that you can either Hit or Stand.\n" \
                "When the round is over, by clicking reset, the table resets and you can play again.\n" \
                "\n\n"\
                "-------------------------------RULES----------------------------------------\n" \
                "The Standard 52-card pack is used. After every round, the deck is shuffled." \
                "\n\n" \
                "Player attempts to beat the dealer by getting a count as close to 21 as possible, " \
                "without going over 21. " \
                "\n\n" \
                "Ace is worth of 11, face cards are 10 and any other is its pip value." \
                "\n\n" \
                "Minimum bet is 10, no max bet." \
                "\n\n" \
                "At the start of the round, dealer gives two cards to player and one himself. If players count is 21 " \
                "in two cards, it is blackjack. In this case, player is payed one and half times the amount of his " \
                "bet." \
                "\n\n" \
                "After two cards, player can choose to Hit or Stand. If player chooses to Hit he may ask the dealer" \
                " for additional cards, one at a time, until he either decides to Stand on the total " \
                "(if it is 21 or under), or goes bust (if it is over 21). If player goes over 21, dealer wins." \
                "\n\n" \
                "After player decides to Stand and hasn't gone over 21, dealer plays. If the dealers total is 17 " \
                "or more, he must Stand. If the total is 16 or under, he must take a card. He must continue to " \
                "take cards until the total is 17 or more, at which point the dealer must Stand. If dealer goes over " \
                "21 he loses." \
                "\n\n" \
                "After player and dealer both have played and neither went over 21, the winner is whoever has higher " \
                "card count. If it's tie, the player gets his bet back. If player wins he gets his bet doubled. "

        self.__rules = Text(self.__window, wrap=WORD)
        self.__rules_scroll = Scrollbar(self.__window, orient="vertical", command=self.__rules.yview)
        self.__rules.config(yscrollcommand=self.__rules_scroll.set)
        self.__rules.insert(END, rules)

        self.__rules.pack(side=LEFT)
        self.__rules_scroll.pack(side=RIGHT, fill=Y)


class Blackjack:
    def __init__(self):
        self.__window = Tk()
        self.__window.title("Blackjack")
        self.__window.geometry("600x300")
        self.__my_var = IntVar()

        self.__cardImages = []

        # GUI texts
        self.__dealer = Label(self.__window, text="Dealer:", font=("Courier", 10))
        self.__dealer_cardSum = Label(self.__window, text="")

        self.__player = Label(self.__window, text="Player:", font=("Courier", 10))
        self.__player_cardSum = Label(self.__window, text="")

        money_left = "Money left: " + str(money)
        self.__moneyLeft = Label(self.__window, text=money_left, font=("Courier", 10))
        self.__bet = Label(self.__window, text="Bet: 10", font=("Courier", 10))

        self.__result = Label(self.__window, text="")

        self.__dealer.grid(row=0, column=0)
        self.__dealer_cardSum.grid(row=0, column=1, sticky=W)
        self.__player.grid(row=6, column=0)
        self.__player_cardSum.grid(row=6, column=1, sticky=W)
        self.__moneyLeft.grid(row=2, column=4, sticky=W)
        self.__bet.grid(row=3, column=4, sticky=W)
        self.__result.grid(row=3, column=0, columnspan=3, sticky=W)

        # GUI Buttons
        self.__deal = Button(self.__window, text="Deal", command=self.play_game)
        self.__reset = Button(self.__window, text="Reset", command=self.reset_game, state=DISABLED)
        self.__betMore = Button(self.__window, text="+10", command=self.add_bet)
        self.__betLess = Button(self.__window, text="-10", command=self.remove_bet)
        self.__Hit = Button(self.__window, text="Hit", command=self.hit, state=DISABLED)
        self.__Stand = Button(self.__window, text="Stand", command=self.stand, state=DISABLED)
        self.__cash_out = Button(self.__window, text="Cash out", command=self.cash)
        self.__rules = Button(self.__window, text="Rules", command=Rules)

        self.__deal.grid(row=2, column=0)
        self.__reset.grid(row=2, column=1)
        self.__betMore.grid(row=4, column=4, sticky=W)
        self.__betLess.grid(row=4, column=4)
        self.__Hit.grid(row=4, column=0)
        self.__Stand.grid(row=4, column=1)
        self.__cash_out.grid(row=6, column=6)
        self.__rules.grid(row=0, column=6)

        # Radio buttons
        self.__sound_on = Radiobutton(self.__window, text="Sound on", variable=self.__my_var,
                                      value=1, command=self.sounds)
        self.__sound_off = Radiobutton(self.__window, text="Sound off", variable=self.__my_var,
                                       value=0, command=self.sounds)

        self.__sound_on.grid(row=0, column=7)
        self.__sound_off.grid(row=0, column=8)

        # GUI Images
        self.__dealer_cards = []
        for i in range(0, 7):
            new_label = Label(self.__window, height=5)
            new_label.grid(row=1, column=0 + i, sticky=E)
            self.__dealer_cards.append(new_label)

        self.__player_cards = []
        for i in range(0, 7):
            new_label = Label(self.__window, height=5)
            new_label.grid(row=5, column=0 + i, sticky=W)
            self.__player_cards.append(new_label)

        # GUI shuffle. Takes every frame from the gif and appends them to the list.
        self.__frames = [PhotoImage(file="shuffle_cards.gif", format="gif -index {}".format(i)) for i in range(10)]

    def play_dealer_cards(self):
        """ Picks cards for dealer and counts the value. """
        global dealer_hand_value
        dealer_card = random.choice(cards)
        while dealer_card in played_cards:
            dealer_card = random.choice(cards)

        card_image = PhotoImage(file=dealer_card)
        self.__cardImages.append(card_image)
        played_cards.append(dealer_card)
        self.place_dealer_cards(card_image)

        card_info = dealer_card.split("_")
        try:
            card_value = int(card_info[0])
            dealer_hand_value += card_value
        except ValueError:
            card_value = (1, 11)
            dealer_hand_value += card_value[1]
        self.__dealer_cardSum.configure(text=dealer_hand_value)
        return dealer_hand_value

    def place_dealer_cards(self, card):
        """ Places the cards on the window and plays sounds if sounds are on. """
        global Dealer_card_number
        self.__dealer_cards[Dealer_card_number - 1].configure(image=card, height=0)
        Dealer_card_number += 1
        self.__window.update_idletasks()

        if self.sounds():
            winsound.PlaySound("cardPlace1.wav", winsound.SND_FILENAME)
            self.__window.update_idletasks()

    def play_player_cards(self):
        """ Picks cards for player and counts the value. """
        global player_hand_value
        player_card = random.choice(cards)
        while player_card in played_cards:
            player_card = random.choice(cards)

        card_image = PhotoImage(file=player_card)
        self.__cardImages.append(card_image)
        played_cards.append(player_card)
        self.place_player_cards(card_image)

        card_info = player_card.split("_")
        try:
            card_value = int(card_info[0])
            player_hand_value += card_value
        except ValueError:
            card_value = (1, 11)
            player_hand_value += card_value[1]
        self.__player_cardSum.configure(text=player_hand_value)
        return player_hand_value

    def place_player_cards(self, card):
        """ Places the cards on the window and plays sounds if sounds are on. """
        global Player_card_number
        self.__player_cards[Player_card_number - 1].configure(image=card, height=0)
        Player_card_number += 1
        self.__window.update_idletasks()

        if self.sounds() and Player_card_number > 1:
            winsound.PlaySound("cardPlace1.wav", winsound.SND_FILENAME)

    def hit(self):
        cont = self.play_player_cards()
        if cont > 21:
            self.__result.configure(text="You lost.", foreground="red")
            self.__deal.configure(state=DISABLED)
            self.__Hit.configure(state=DISABLED)
            self.__Stand.configure(state=DISABLED)
            self.__reset.configure(state=NORMAL)

    def stand(self):
        global money, bet
        cont = self.play_dealer_cards()
        self.__window.update_idletasks()
        while cont < 17:
            cont = self.play_dealer_cards()
            time.sleep(1)
            self.__window.update_idletasks()

        if cont > 21:
            self.__result.configure(text="You win.", foreground="blue")
            money += bet*2
            money_left = "Money left: " + str(money)
            self.__moneyLeft.configure(text=money_left)
            self.__deal.configure(state=DISABLED)
            self.__Hit.configure(state=DISABLED)
            self.__Stand.configure(state=DISABLED)
            self.__reset.configure(state=NORMAL)
        else:
            self.check_result()

    def add_bet(self):
        global bet
        bet += 10
        amount = "Bet: " + str(bet)
        self.__bet.configure(text=amount)
        if self.sounds():
            winsound.PlaySound("chipLay1.wav", winsound.SND_FILENAME)

    def remove_bet(self):
        global bet
        if bet - 10 >= 0:
            bet -= 10
        amount = "Bet: " + str(bet)
        self.__bet.configure(text=amount)
        if self.sounds():
            winsound.PlaySound("chipLay3.wav", winsound.SND_FILENAME)

    def check_result(self):
        """ If player nor dealer went over 21, the program checks which one has higher hand value. """
        global money, bet
        if player_hand_value > dealer_hand_value:
            self.__result.configure(text="You win.", foreground="blue")
            money += bet * 2
            money_left = "Money left: " + str(money)
            self.__moneyLeft.configure(text=money_left)
        elif player_hand_value == dealer_hand_value:
            self.__result.configure(text="Tie.", foreground="blue")
            money += bet
            money_left = "Money left: " + str(money)
            self.__moneyLeft.configure(text=money_left)
        else:
            self.__result.configure(text="You lost.", foreground="red")

        self.__deal.configure(state=DISABLED)
        self.__Hit.configure(state=DISABLED)
        self.__Stand.configure(state=DISABLED)
        self.__reset.configure(state=NORMAL)

    def reset_game(self):
        """ Resets the game for next round. Removes all cards from the table and sets hand values to zero. """
        global played_cards, Dealer_card_number, Player_card_number, dealer_hand_value, player_hand_value
        played_cards = []
        for i in range(1, 7):
            self.__dealer_cards[i-1].configure(image="")
            self.__player_cards[i-1].configure(image="")

        self.__result.configure(text="")
        self.__player_cardSum.configure(text="")
        self.__dealer_cardSum.configure(text="")

        Dealer_card_number = 1
        Player_card_number = 1
        dealer_hand_value = 0
        player_hand_value = 0

        self.__deal.configure(state=NORMAL)
        self.__Hit.configure(state=NORMAL)
        self.__Stand.configure(state=NORMAL)
        self.__Hit.configure(state=DISABLED)
        self.__Stand.configure(state=DISABLED)
        self.__reset.configure(state=DISABLED)

        for n in range(2):
            for i in self.__frames:
                self.__dealer_cards[0].grid(columnspan=2)
                self.__dealer_cards[0].configure(image=i)
                self.__window.update_idletasks()
                time.sleep(0.1)
        self.__dealer_cards[0].configure(image="")
        self.__dealer_cards[0].grid(columnspan=1)
        self.__window.update_idletasks()

    def cash(self):
        self.__window.destroy()
        EndingMoney()

    def play_game(self):
        """ When user presses "deal" the program checks the bet and deals first cards on the table. """
        global money, bet
        if int(money) - bet < 0:
            self.__result.configure(text="Not enough money.")
            return
        elif bet == 0:
            self.__result.configure(text="Bet can't be 0.")
            return

        self.__result.configure(text="")
        money = int(money) - bet
        money_left = "Money left: " + str(money)
        self.__moneyLeft.configure(text=money_left)

        self.__deal.configure(state=DISABLED)
        self.__reset.configure(state=DISABLED)
        self.__Hit.configure(state=NORMAL)
        self.__Stand.configure(state=NORMAL)

        self.play_player_cards()
        self.play_dealer_cards()
        self.play_player_cards()
        if player_hand_value == 21:
            self.__result.configure(text="You win.", foreground="blue")
            money += bet * 2.5
            money_left = "Money left: " + str(money)
            self.__moneyLeft.configure(text=money_left)
            self.__deal.configure(state=DISABLED)
            self.__Hit.configure(state=DISABLED)
            self.__Stand.configure(state=DISABLED)
            self.__reset.configure(state=NORMAL)

    def sounds(self):
        if self.__my_var.get() == 1:
            return True
        elif self.__my_var.get() == 0:
            return False

    def start(self):
        self.__window.mainloop()


def main():
    StartingMoney()
main()
